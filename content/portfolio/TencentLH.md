---
title: "【玩转Lighthouse】厌倦了服务器搬家？利用“薅”来的轻量搭建K3s集群"
date: 2022-06-12T22:08:45+08:00
draft: false
image: "img/portfolio/tencent.png"
---

# Please Click the Link Below

[【玩转Lighthouse】厌倦了服务器搬家？利用“薅”来的轻量搭建K3s集群 - 云+社区 - 腾讯云 (tencent.com)](https://cloud.tencent.com/developer/article/1979073)
